const express = require("express");
const app = express();
const http = require("http");
const cors = require("cors");
app.use(cors("*"));
const PORT = process.env.PORT || 5000;
const server = http.createServer(app);
const { Server } = require("socket.io");

const io = new Server(server, {
  cors: {
    origin: true,
    methods: ["GET", "POST"],
  },
});
app.get("/", (req, res) => {
  res.send("Hello World!");
});
//
const clientRooms = [];

io.on("connection", (client) => {
  let allUsers = [];

  function handleJoinGame(roomName) {
    client.emit("id", roomName);
    const room = io.sockets.adapter.rooms[roomName];
    if (room.length === 4) {
      client.emit("maxplayer", "full room");
    }
    if (room) {
      allUsers.push(room.length);
    }
    clientRooms[client.id] = roomName;
    client.join(roomName);
    client.emit("joinerColor", {
      idClient: client.id,
      nbClient: allUsers,
    });
  }

  function handleNewGame() {
    let roomName = makeid(5);
    client.emit("roomName", roomName);
    clientRooms[client.id] = roomName;

    client.emit("gameCode", roomName);
    client.join(roomName);
    // client.number = 1;
    client.emit("createrColor", client.id);
  }

  client.on("newGame", handleNewGame);
  client.on("joinGame", handleJoinGame);

  client.on("moved", (data) => {
    client.to(data.idroom).emit("clientToClient", {
      board: data.board,
      firstPlayer: data.firstPlayer,
      Green: data.playerGreen,
      Red: data.playerRed,
      Blue: data.playerBlue,
      Yellow: data.playerYellow,
    });
  });

  client.on("gameOver", (data) => {
    console.log(data);
    io.to(data.idroom).emit("gameOverToClient", data);
  });
});

function makeid(length) {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
server.listen(PORT, () => console.log(`server listen on PORT : ${PORT}`));
