import { io } from "socket.io-client";
import "./style.css";

let boss = [
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

let socket = io("http://localhost:5000", {
  withCredentials: true,
});

function listToMatrix(list, elementsPerSubArray) {
  var matrix = [],
    i,
    k;

  for (i = 0, k = -1; i < list.length; i++) {
    if (i % elementsPerSubArray === 0) {
      k++;
      matrix[k] = [];
    }

    matrix[k].push(list[i]);
  }

  return matrix;
}

function RealMatrix(matrix) {
  let newMatrix = [];

  let params = { rows: [], startJ: Infinity, endJ: -Infinity };

  for (let i = 0; i < matrix.length; i++) {
    let row = matrix[i];

    let firstOne = row.indexOf(1);

    let lastOne = row.lastIndexOf(1);

    if (firstOne !== -1) {
      params.rows.push(i);
    }

    if (params.startJ > firstOne && firstOne !== -1) {
      params.startJ = firstOne;
    }

    if (params.endJ < lastOne && lastOne !== -1) {
      params.endJ = lastOne;
    }
  }

  for (let i = 0; i < matrix.length; i++) {
    if (params.rows.includes(i)) {
      newMatrix.push(matrix[i].slice(params.startJ, params.endJ + 1));
    }
  }

  return newMatrix;
}

function rotateMatrix(a) {
  let newM = [...a.map((elem) => [...elem])];

  let y = newM.length - 1;
  for (let i = 0; i < newM.length / 2; i++) {
    let x = newM.length - 1 - i;
    for (let j = i; j < y; j++) {
      let t = newM[i][j];
      newM[i][j] = newM[j][y];
      newM[j][y] = t;
      t = newM[i][j];
      newM[i][j] = newM[y][x];
      newM[y][x] = t;
      t = newM[i][j];
      newM[i][j] = newM[x][i];
      newM[x][i] = t;
      x--;
    }
    y--;
  }
  return newM;
}

let flipMatrix = (matrix) => {
  let newM = [...matrix.map((elem) => [...elem])];

  for (let i = 0; i < newM.length; i++) {
    for (let j = 0; j < Math.floor(newM[i].length / 2); j++) {
      [newM[i][j], newM[i][newM[i].length - j - 1]] = [
        newM[i][newM[i].length - j - 1],
        newM[i][j],
      ];
    }
  }
  return newM;
};

let matrix1 = [
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
];

let matrix2 = [
  [0, 0, 0, 0, 0],
  [0, 0, 1, 1, 0],
  [0, 0, 1, 1, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix3 = [
  [0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0],
  [0, 0, 1, 1, 0],
  [0, 0, 0, 0, 0],
];
let matrix4 = [
  [1, 1, 1, 0, 0],
  [0, 1, 0, 0, 0],
  [0, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix5 = [
  [0, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 1, 1, 0, 0],
  [0, 0, 1, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix6 = [
  [1, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix7 = [
  [1, 0, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [0, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix8 = [
  [1, 1, 0, 0, 0],
  [0, 1, 1, 0, 0],
  [0, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix9 = [
  [1, 0, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix10 = [
  [1, 1, 1, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix11 = [
  [1, 0, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [0, 1, 0, 0, 0],
  [0, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix12 = [
  [1, 0, 1, 0, 0],
  [1, 1, 1, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix13 = [
  [1, 0, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [0, 1, 1, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix14 = [
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix15 = [
  [0, 0, 0, 0, 0],
  [0, 0, 1, 0, 0],
  [0, 1, 1, 1, 0],
  [0, 0, 1, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix16 = [
  [1, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix17 = [
  [1, 1, 1, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];

let matrix18 = [
  [1, 0, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix19 = [
  [1, 0, 0, 0, 0],
  [1, 1, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let matrix20 = [
  [0, 0, 0, 0, 0],
  [0, 0, 1, 0, 0],
  [0, 0, 1, 0, 0],
  [0, 0, 1, 1, 0],
  [0, 0, 0, 0, 0],
];
let matrix21 = [
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [1, 0, 0, 0, 0],
  [0, 0, 0, 0, 0],
];
let thechosen = [];

let firstPlayer = 0;
let player = document.getElementById(`tetris${firstPlayer}`);
player.classList.remove("notThePlayer");

//CREATE SHAPS
function shapes(m, tetris, shapColor, theoneshap) {
  let oneshap = document.createElement("div");

  oneshap.setAttribute("id", m);

  oneshap.classList.add(theoneshap);
  for (let r = 0; r < m.length; r++) {
    for (let c = 0; c < m.length; c++) {
      if (m[r][c] !== 0 || undefined) {
        let shap = document.createElement("span");
        shap.id = "SH" + r.toString() + "-" + c.toString();
        shap.classList.add(shapColor);
        oneshap.appendChild(shap);
      } else {
        let shapNone = document.createElement("span");
        shapNone.classList.add("shapNone");
        oneshap.appendChild(shapNone);
      }
    }
  }
  document.getElementById(tetris).appendChild(oneshap);
}

shapes(matrix1, "tetris0", "shap0", "oneshap0");
shapes(matrix2, "tetris0", "shap0", "oneshap0");
shapes(matrix3, "tetris0", "shap0", "oneshap0");
shapes(matrix4, "tetris0", "shap0", "oneshap0");
shapes(matrix5, "tetris0", "shap0", "oneshap0");
shapes(matrix6, "tetris0", "shap0", "oneshap0");
shapes(matrix7, "tetris0", "shap0", "oneshap0");
shapes(matrix8, "tetris0", "shap0", "oneshap0");
shapes(matrix9, "tetris0", "shap0", "oneshap0");
shapes(matrix10, "tetris0", "shap0", "oneshap0");
shapes(matrix11, "tetris0", "shap0", "oneshap0");
shapes(matrix12, "tetris0", "shap0", "oneshap0");
shapes(matrix13, "tetris0", "shap0", "oneshap0");
shapes(matrix14, "tetris0", "shap0", "oneshap0");
shapes(matrix15, "tetris0", "shap0", "oneshap0");
shapes(matrix16, "tetris0", "shap0", "oneshap0");
shapes(matrix17, "tetris0", "shap0", "oneshap0");
shapes(matrix18, "tetris0", "shap0", "oneshap0");
shapes(matrix19, "tetris0", "shap0", "oneshap0");
shapes(matrix20, "tetris0", "shap0", "oneshap0");
shapes(matrix21, "tetris0", "shap0", "oneshap0");

shapes(matrix1, "tetris1", "shap1", "oneshap1");
shapes(matrix2, "tetris1", "shap1", "oneshap1");
shapes(matrix3, "tetris1", "shap1", "oneshap1");
shapes(matrix4, "tetris1", "shap1", "oneshap1");
shapes(matrix5, "tetris1", "shap1", "oneshap1");
shapes(matrix6, "tetris1", "shap1", "oneshap1");
shapes(matrix7, "tetris1", "shap1", "oneshap1");
shapes(matrix8, "tetris1", "shap1", "oneshap1");
shapes(matrix9, "tetris1", "shap1", "oneshap1");
shapes(matrix10, "tetris1", "shap1", "oneshap1");
shapes(matrix11, "tetris1", "shap1", "oneshap1");
shapes(matrix12, "tetris1", "shap1", "oneshap1");
shapes(matrix13, "tetris1", "shap1", "oneshap1");
shapes(matrix14, "tetris1", "shap1", "oneshap1");
shapes(matrix15, "tetris1", "shap1", "oneshap1");
shapes(matrix16, "tetris1", "shap1", "oneshap1");
shapes(matrix17, "tetris1", "shap1", "oneshap1");
shapes(matrix18, "tetris1", "shap1", "oneshap1");
shapes(matrix19, "tetris1", "shap1", "oneshap1");
shapes(matrix20, "tetris1", "shap1", "oneshap1");
shapes(matrix21, "tetris1", "shap1", "oneshap1");

shapes(matrix1, "tetris2", "shap2", "oneshap2");
shapes(matrix2, "tetris2", "shap2", "oneshap2");
shapes(matrix3, "tetris2", "shap2", "oneshap2");
shapes(matrix4, "tetris2", "shap2", "oneshap2");
shapes(matrix5, "tetris2", "shap2", "oneshap2");
shapes(matrix6, "tetris2", "shap2", "oneshap2");
shapes(matrix7, "tetris2", "shap2", "oneshap2");
shapes(matrix8, "tetris2", "shap2", "oneshap2");
shapes(matrix9, "tetris2", "shap2", "oneshap2");
shapes(matrix10, "tetris2", "shap2", "oneshap2");
shapes(matrix11, "tetris2", "shap2", "oneshap2");
shapes(matrix12, "tetris2", "shap2", "oneshap2");
shapes(matrix13, "tetris2", "shap2", "oneshap2");
shapes(matrix14, "tetris2", "shap2", "oneshap2");
shapes(matrix15, "tetris2", "shap2", "oneshap2");
shapes(matrix16, "tetris2", "shap2", "oneshap2");
shapes(matrix17, "tetris2", "shap2", "oneshap2");
shapes(matrix18, "tetris2", "shap2", "oneshap2");
shapes(matrix19, "tetris2", "shap2", "oneshap2");
shapes(matrix20, "tetris2", "shap2", "oneshap2");
shapes(matrix21, "tetris2", "shap2", "oneshap2");

shapes(matrix1, "tetris3", "shap3", "oneshap3");
shapes(matrix2, "tetris3", "shap3", "oneshap3");
shapes(matrix3, "tetris3", "shap3", "oneshap3");
shapes(matrix4, "tetris3", "shap3", "oneshap3");
shapes(matrix5, "tetris3", "shap3", "oneshap3");
shapes(matrix6, "tetris3", "shap3", "oneshap3");
shapes(matrix7, "tetris3", "shap3", "oneshap3");
shapes(matrix8, "tetris3", "shap3", "oneshap3");
shapes(matrix9, "tetris3", "shap3", "oneshap3");
shapes(matrix10, "tetris3", "shap3", "oneshap3");
shapes(matrix11, "tetris3", "shap3", "oneshap3");
shapes(matrix12, "tetris3", "shap3", "oneshap3");
shapes(matrix13, "tetris3", "shap3", "oneshap3");
shapes(matrix14, "tetris3", "shap3", "oneshap3");
shapes(matrix15, "tetris3", "shap3", "oneshap3");
shapes(matrix16, "tetris3", "shap3", "oneshap3");
shapes(matrix17, "tetris3", "shap3", "oneshap3");
shapes(matrix18, "tetris3", "shap3", "oneshap3");
shapes(matrix19, "tetris3", "shap3", "oneshap3");
shapes(matrix20, "tetris3", "shap3", "oneshap3");
shapes(matrix21, "tetris3", "shap3", "oneshap3");

let theShapChoseen = (oneshap) => {
  let CpoyMatrix = document.querySelectorAll(oneshap);

  CpoyMatrix.forEach((elem) =>
    elem.addEventListener(
      "click",
      function () {
        let arrOfNum = [];
        let arrOfStr = [];
        arrOfStr = elem.id.split(",");
        arrOfStr.forEach((str) => {
          arrOfNum.push(Number(str));
          thechosen = listToMatrix(arrOfNum, 5);
        });

        document.addEventListener("click", function (e) {
          if (e.target.classList.contains("flip")) {
            thechosen = flipMatrix(thechosen);
          }
        });

        document.addEventListener("click", function (e) {
          if (e.target.classList.contains("rotate")) {
            thechosen = rotateMatrix(thechosen);
          }
        });

        // socket.emit("classIdShap",
        // {
        //   idroom:document.getElementById("gameCodeDisplay").innerText,
        //   idShap:elem.id,
        //   classShap:elem.classList[0]
        // }
        // );
        // socket.on("ToClient", (data) => {
        //   console.log(data);
        //   idShap=data.idShap,
        //   classShap=data.classShap
        // })

        // elem.classList.add("not-allowed");
      },
      { once: true }
    )
  );
};
let first;
let nbRed = 0,
  nbYellow = 0,
  nbBlue = 0,
  nbGreen = 0;

let drawHover = function (e) {
  if (e.target.classList.contains("tile")) {
    let tile = e.target;
    let color;

    switch (firstPlayer) {
      case 0:
        color = "Colorred";
        break;
      case 1:
        color = "Colorgreen";
        break;
      case 2:
        color = "Colorblue";
        break;
      case 3:
        color = "Coloryellow";
        break;
    }

    let posI = Number(tile.id.split("-")[0]);
    let posJ = Number(tile.id.split("-")[1]);
    let intiposJ = posJ;
    let intiposI = posI;

    let IsTestColor = true;

    let newM = RealMatrix([...thechosen.map((elem) => [...elem])]);

    for (let i = 0; i < newM.length; i++) {
      for (let j = 0; j < newM[i].length; j++) {
        if (newM[i][j] !== 0) {
          let boxshap = document.getElementById(`${posI}-${posJ}`);

          if (boxshap && boxshap.classList.contains("checked")) {
            IsTestColor = false;

            return;
          }

          posJ = posJ + 1;
          if (posJ > 21) {
            IsTestColor = false;

            return;
          }
        } else {
          posJ = posJ + 1;
          if (posJ > 21) {
            IsTestColor = false;

            return;
          }
        }
      }
      posJ = intiposJ;
      posI = posI + 1;
      if (posI > 21) {
        IsTestColor = false;

        return;
      }
    }
    if (IsTestColor === true) {
      posJ = intiposJ;
      posI = intiposI;
      for (let i = 0; i < newM.length; i++) {
        for (let j = 0; j < newM[i].length; j++) {
          if (newM[i][j] !== 0) {
            let boxshap = document.getElementById(`${posI}-${posJ}`);
            boxshap.classList.add(`hover-${color}`);

            posJ = posJ + 1;
          } else {
            posJ = posJ + 1;
          }
        }
        posJ = intiposJ;
        posI = posI + 1;
      }
    }

    newM = [];
  }
};

let undrawOut = function (e) {
  if (e.target.classList.contains("tile")) {
    [...document.querySelectorAll(".tile")].forEach((elem) => {
      elem.classList.forEach((className) => {
        if (className.includes("hover")) {
          elem.classList.remove(className);
        }
      });
    });
  }
};
let checkAndDraw = function (e) {
  let nbRed = Number(document.getElementById("playerRed").innerText);
  let nbGreen = Number(document.getElementById("playerGreen").innerText);
  let nbBlue = Number(document.getElementById("playerBlue").innerText);
  let nbYellow = Number(document.getElementById("playerYellow").innerText);
  if (e.target.classList.contains("tile")) {
    let tile = e.target;
    let color;

    switch (firstPlayer) {
      case 0:
        color = "Colorred";
        break;
      case 1:
        color = "Colorgreen";
        break;
      case 2:
        color = "Colorblue";
        break;
      case 3:
        color = "Coloryellow";
        break;
    }

    let posI = Number(tile.id.split("-")[0]);
    let posJ = Number(tile.id.split("-")[1]);
    let intiposJ = posJ;
    let intiposI = posI;
    let IsTestColor = true;
    let validPlace = false;
    let newM = RealMatrix([...thechosen.map((elem) => [...elem])]);

    for (let i = 0; i < newM.length; i++) {
      for (let j = 0; j < newM[i].length; j++) {
        if (newM[i][j] !== 0) {
          let boxshap = document.getElementById(`${posI}-${posJ}`);

          if (posJ > 20 || posI > 20) {
            IsTestColor = false;
            alert("wrong place");
            return;
          }

          //8alet bjnab o5tha
          if (posI === 1 && posJ === 1) {
            let isTop = document
              .getElementById(`${posI}-${posJ + 1}`)
              .classList.contains(color);
            let isRight = document
              .getElementById(`${posI + 1}-${posJ}`)
              .classList.contains(color);
            if (isRight || isTop) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else if (posI === 20 && posJ === 20) {
            let isLeft = document
              .getElementById(`${posI}-${posJ - 1}`)
              .classList.contains(color);
            let isBottom = document
              .getElementById(`${posI - 1}-${posJ}`)
              .classList.contains(color);
            if (isLeft || isBottom) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else if (posI === 1 && posJ === 20) {
            let isLeft = document
              .getElementById(`${posI}-${posJ - 1}`)
              .classList.contains(color);
            let isRight = document
              .getElementById(`${posI + 1}-${posJ}`)
              .classList.contains(color);
            if (isRight || isLeft) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else if (posI === 20 && posJ === 1) {
            let isBottom = document
              .getElementById(`${posI - 1}-${posJ}`)
              .classList.contains(color);
            let isTop = document
              .getElementById(`${posI}-${posJ + 1}`)
              .classList.contains(color);

            if (isTop || isBottom) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else if (posJ === 20) {
            let isBottom = document
              .getElementById(`${posI - 1}-${posJ}`)
              .classList.contains(color);
            let isRight = document
              .getElementById(`${posI + 1}-${posJ}`)
              .classList.contains(color);
            let isLeft = document
              .getElementById(`${posI}-${posJ - 1}`)
              .classList.contains(color);
            if (isBottom || isRight || isLeft) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else if (posJ === 1) {
            let isBottom = document
              .getElementById(`${posI - 1}-${posJ}`)
              .classList.contains(color);
            let isRight = document
              .getElementById(`${posI + 1}-${posJ}`)
              .classList.contains(color);
            let isTop = document
              .getElementById(`${posI}-${posJ + 1}`)
              .classList.contains(color);
            if (isBottom || isRight || isTop) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else if (posI === 1) {
            let isLeft = document
              .getElementById(`${posI}-${posJ - 1}`)
              .classList.contains(color);
            let isTop = document
              .getElementById(`${posI}-${posJ + 1}`)
              .classList.contains(color);
            let isRight = document
              .getElementById(`${posI + 1}-${posJ}`)
              .classList.contains(color);
            if (isLeft || isRight || isTop) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else if (posI === 20) {
            let isLeft = document
              .getElementById(`${posI}-${posJ - 1}`)
              .classList.contains(color);
            let isBottom = document
              .getElementById(`${posI - 1}-${posJ}`)
              .classList.contains(color);
            let isTop = document
              .getElementById(`${posI}-${posJ + 1}`)
              .classList.contains(color);

            if (isLeft || isBottom || isTop) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          } else {
            let isLeft = document
              .getElementById(`${posI}-${posJ - 1}`)
              .classList.contains(color);
            let isBottom = document
              .getElementById(`${posI - 1}-${posJ}`)
              .classList.contains(color);
            let isTop = document
              .getElementById(`${posI}-${posJ + 1}`)
              .classList.contains(color);
            let isRight = document
              .getElementById(`${posI + 1}-${posJ}`)
              .classList.contains(color);

            if (isLeft || isBottom || isRight || isTop) {
              IsTestColor = false;
              alert("there is shap here !!!!");
              return;
            }
          }

          //mich fel corner 7tha o5tha
          if (boxshap.classList.contains(color.slice(5))) {
            validPlace = true;
          } else if (posI === 1 && posJ === 1) {
            let isTopRightFilled = document
              .getElementById(`${posI + 1}-${posJ + 1}`)
              .classList.contains(color);
            if (isTopRightFilled) {
              validPlace = true;
            }
          } else if (posI === 20 && posJ === 20) {
            let isTopLeftFilled = document
              .getElementById(`${posI - 1}-${posJ - 1}`)
              .classList.contains(color);
            if (isTopLeftFilled) {
              validPlace = true;
            }
          } else if (posI === 1 && posJ === 20) {
            let isBottomLeftFilled = document
              .getElementById(`${posI + 1}-${posJ - 1}`)
              .classList.contains(color);

            if (isBottomLeftFilled) {
              validPlace = true;
            }
          } else if (posI === 20 && posJ === 1) {
            let isBottomRightFilled = document
              .getElementById(`${posI - 1}-${posJ + 1}`)
              .classList.contains(color);
            if (isBottomRightFilled) {
              validPlace = true;
            }
          } else if (posJ === 20) {
            let isTopLeftFilled = document
              .getElementById(`${posI - 1}-${posJ - 1}`)
              .classList.contains(color);
            let isBottomLeftFilled = document
              .getElementById(`${posI + 1}-${posJ - 1}`)
              .classList.contains(color);
            if (isTopLeftFilled || isBottomLeftFilled) {
              validPlace = true;
            }
          } else if (posJ === 1) {
            let isBottomRightFilled = document
              .getElementById(`${posI - 1}-${posJ + 1}`)
              .classList.contains(color);
            let isTopRightFilled = document
              .getElementById(`${posI + 1}-${posJ + 1}`)
              .classList.contains(color);
            if (isBottomRightFilled || isTopRightFilled) {
              validPlace = true;
            }
          } else if (posI === 1) {
            let isBottomLeftFilled = document
              .getElementById(`${posI + 1}-${posJ - 1}`)
              .classList.contains(color);
            let isTopRightFilled = document
              .getElementById(`${posI + 1}-${posJ + 1}`)
              .classList.contains(color);

            if (isBottomLeftFilled || isTopRightFilled) {
              validPlace = true;
            }
          } else if (posI === 20) {
            let isTopLeftFilled = document
              .getElementById(`${posI - 1}-${posJ - 1}`)
              .classList.contains(color);
            let isBottomRightFilled = document
              .getElementById(`${posI - 1}-${posJ + 1}`)
              .classList.contains(color);

            if (isTopLeftFilled || isBottomRightFilled) {
              validPlace = true;
            }
          } else {
            let isTopLeftFilled = document
              .getElementById(`${posI - 1}-${posJ - 1}`)
              .classList.contains(color);
            let isBottomLeftFilled = document
              .getElementById(`${posI + 1}-${posJ - 1}`)
              .classList.contains(color);
            let isBottomRightFilled = document
              .getElementById(`${posI - 1}-${posJ + 1}`)
              .classList.contains(color);
            let isTopRightFilled = document
              .getElementById(`${posI + 1}-${posJ + 1}`)
              .classList.contains(color);
            if (
              isTopLeftFilled ||
              isBottomLeftFilled ||
              isBottomRightFilled ||
              isTopRightFilled
            ) {
              validPlace = true;
            }
          }

          if (boxshap.classList.contains("checked")) {
            IsTestColor = false;
            alert("there is one in this place !!!!!");
            return;
          }

          posJ = posJ + 1;
          if (posJ > 21) {
            IsTestColor = false;
            alert("wrong place");
            return;
          }
        } else {
          posJ = posJ + 1;
          if (posJ > 21) {
            IsTestColor = false;
            alert("wrong place");
            return;
          }
        }
      }
      posJ = intiposJ;
      posI = posI + 1;
      if (posI > 21) {
        IsTestColor = false;
        alert("wrong place");
        return;
      }
    }

    if (validPlace === false) {
      alert("this not your place");
      return;
    }

    if (IsTestColor === true && validPlace === true) {
      posJ = intiposJ;
      posI = intiposI;

      for (let i = 0; i < newM.length; i++) {
        for (let j = 0; j < newM[i].length; j++) {
          if (newM[i][j] !== 0) {
            let boxshap = document.getElementById(`${posI}-${posJ}`);
            boxshap.classList.remove("Colorgreen");
            boxshap.classList.remove("Colorred");
            boxshap.classList.remove("Colorblue");
            boxshap.classList.remove("Coloryellow");
            boxshap.setAttribute("color", color);
            boxshap.classList.add("checked");
            boxshap.classList.add(color);

            if (color === "Colorred") {
              nbRed = nbRed + 1;
              boss[posI - 1][posJ - 1] = 1;
            }
            if (color === "Coloryellow") {
              nbYellow = nbYellow + 1;
              boss[posI - 1][posJ - 1] = 4;
            }
            if (color === "Colorblue") {
              nbBlue = nbBlue + 1;
              boss[posI - 1][posJ - 1] = 3;
            }
            if (color === "Colorgreen") {
              nbGreen = nbGreen + 1;
              boss[posI - 1][posJ - 1] = 2;
            }

            posJ = posJ + 1;
          } else {
            posJ = posJ + 1;
          }
        }
        posJ = intiposJ;
        posI = posI + 1;
      }
    }
    if (firstPlayer < 3) {
      firstPlayer = firstPlayer + 1;
      if (firstPlayer === 0) {
        document.getElementById(`tetris0`).classList.remove("notThePlayer");
        document.getElementById(`tetris1`).classList.add("notThePlayer");
        document.getElementById(`tetris2`).classList.add("notThePlayer");
        document.getElementById(`tetris3`).classList.add("notThePlayer");
      }
      if (firstPlayer === 1) {
        document.getElementById(`tetris1`).classList.remove("notThePlayer");
        document.getElementById(`tetris0`).classList.add("notThePlayer");
        document.getElementById(`tetris2`).classList.add("notThePlayer");
        document.getElementById(`tetris3`).classList.add("notThePlayer");
      }
      if (firstPlayer === 2) {
        document.getElementById(`tetris2`).classList.remove("notThePlayer");
        document.getElementById(`tetris0`).classList.add("notThePlayer");
        document.getElementById(`tetris1`).classList.add("notThePlayer");
        document.getElementById(`tetris3`).classList.add("notThePlayer");
      }
      if (firstPlayer === 3) {
        document.getElementById(`tetris3`).classList.remove("notThePlayer");
        document.getElementById(`tetris0`).classList.add("notThePlayer");
        document.getElementById(`tetris1`).classList.add("notThePlayer");
        document.getElementById(`tetris2`).classList.add("notThePlayer");
      }
    } else if (firstPlayer === 3) {
      firstPlayer = 0;
      if (firstPlayer === 0) {
        document.getElementById(`tetris0`).classList.remove("notThePlayer");
        document.getElementById(`tetris1`).classList.add("notThePlayer");
        document.getElementById(`tetris2`).classList.add("notThePlayer");
        document.getElementById(`tetris3`).classList.add("notThePlayer");
      }
      if (firstPlayer === 1) {
        document.getElementById(`tetris1`).classList.remove("notThePlayer");
        document.getElementById(`tetris0`).classList.add("notThePlayer");
        document.getElementById(`tetris2`).classList.add("notThePlayer");
        document.getElementById(`tetris3`).classList.add("notThePlayer");
      }
      if (firstPlayer === 2) {
        document.getElementById(`tetris2`).classList.remove("notThePlayer");
        document.getElementById(`tetris0`).classList.add("notThePlayer");
        document.getElementById(`tetris1`).classList.add("notThePlayer");
        document.getElementById(`tetris3`).classList.add("notThePlayer");
      }
      if (firstPlayer === 3) {
        document.getElementById(`tetris3`).classList.remove("notThePlayer");
        document.getElementById(`tetris0`).classList.add("notThePlayer");
        document.getElementById(`tetris1`).classList.add("notThePlayer");
        document.getElementById(`tetris2`).classList.add("notThePlayer");
      }
    }
    thechosen = [];
    newM = [];
  }

  // console.log(nbRed,nbGreen,nbBlue,nbYellow)

  let playerRed = document.getElementById("playerRed");
  playerRed.innerText = nbRed;

  let playerYellow = document.getElementById("playerYellow");
  playerYellow.innerText = nbYellow;

  let playerBlue = document.getElementById("playerBlue");
  playerBlue.innerText = nbBlue;

  let playerGreen = document.getElementById("playerGreen");
  playerGreen.innerText = nbGreen;

  socket.emit("moved", {
    idroom: document.getElementById("gameCodeDisplay").innerText,
    board: boss,
    firstPlayer: firstPlayer,
    playerRed: nbRed,
    playerGreen: nbGreen,
    playerBlue: nbBlue,
    playerYellow: nbYellow,
  });

  socket.on("clientToClient", (data) => {
    firstPlayer = data.firstPlayer;
    // console.log(data.Red,data.Green,data.Blue,data.Yellow)
    document.getElementById("playerRed").innerText = data.Red;
    document.getElementById("playerYellow").innerText = data.Yellow;
    document.getElementById("playerBlue").innerText = data.Blue;
    document.getElementById("playerGreen").innerText = data.Green;

    if (firstPlayer === 0) {
      document.getElementById(`tetris0`).classList.remove("notThePlayer");
      document.getElementById(`tetris1`).classList.add("notThePlayer");
      document.getElementById(`tetris2`).classList.add("notThePlayer");
      document.getElementById(`tetris3`).classList.add("notThePlayer");
    }
    if (firstPlayer === 1) {
      document.getElementById(`tetris1`).classList.remove("notThePlayer");
      document.getElementById(`tetris0`).classList.add("notThePlayer");
      document.getElementById(`tetris2`).classList.add("notThePlayer");
      document.getElementById(`tetris3`).classList.add("notThePlayer");
    }
    if (firstPlayer === 2) {
      document.getElementById(`tetris2`).classList.remove("notThePlayer");
      document.getElementById(`tetris0`).classList.add("notThePlayer");
      document.getElementById(`tetris1`).classList.add("notThePlayer");
      document.getElementById(`tetris3`).classList.add("notThePlayer");
    }
    if (firstPlayer === 3) {
      document.getElementById(`tetris3`).classList.remove("notThePlayer");
      document.getElementById(`tetris0`).classList.add("notThePlayer");
      document.getElementById(`tetris1`).classList.add("notThePlayer");
      document.getElementById(`tetris2`).classList.add("notThePlayer");
    }

    boss = data.board;
    for (let i = 0; i < boss.length; i++) {
      for (let j = 0; j < boss[i].length; j++) {
        let newTile = document.getElementById(`${i + 1}-${j + 1}`);
        if (boss[i][j] === 1) {
          newTile.classList.add("checked");
          newTile.classList.add("Colorred");
        } else if (boss[i][j] === 2) {
          newTile.classList.add("checked");
          newTile.classList.add("Colorgreen");
        } else if (boss[i][j] === 3) {
          newTile.classList.add("checked");
          newTile.classList.add("Colorblue");
        } else if (boss[i][j] === 4) {
          newTile.classList.add("checked");
          newTile.classList.add("Coloryellow");
        }
      }
    }
  });

  if (nbRed >= 89) {
    alert("the red win");
    window.location.reload();
  } else if (nbYellow >= 89) {
    alert("the Yellow win");
    window.location.reload();
  } else if (nbBlue >= 89) {
    alert("the Blue win");
    window.location.reload();
  } else if (nbGreen >= 89) {
    alert("the Green win");
    window.location.reload();
  }
};

document.addEventListener("click", function (e) {
  if (e.target.classList.contains(`done`)) {
    if (firstPlayer < 3) {
      firstPlayer = firstPlayer + 1;
    } else if (firstPlayer === 3) {
      firstPlayer = 0;
    }
    if (firstPlayer === 0) {
      document.getElementById(`tetris0`).classList.remove("notThePlayer");
      document.getElementById(`tetris1`).classList.add("notThePlayer");
      document.getElementById(`tetris2`).classList.add("notThePlayer");
      document.getElementById(`tetris3`).classList.add("notThePlayer");
    }
    if (firstPlayer === 1) {
      document.getElementById(`tetris1`).classList.remove("notThePlayer");
      document.getElementById(`tetris0`).classList.add("notThePlayer");
      document.getElementById(`tetris2`).classList.add("notThePlayer");
      document.getElementById(`tetris3`).classList.add("notThePlayer");
    }
    if (firstPlayer === 2) {
      document.getElementById(`tetris2`).classList.remove("notThePlayer");
      document.getElementById(`tetris0`).classList.add("notThePlayer");
      document.getElementById(`tetris1`).classList.add("notThePlayer");
      document.getElementById(`tetris3`).classList.add("notThePlayer");
    }
    if (firstPlayer === 3) {
      document.getElementById(`tetris3`).classList.remove("notThePlayer");
      document.getElementById(`tetris0`).classList.add("notThePlayer");
      document.getElementById(`tetris1`).classList.add("notThePlayer");
      document.getElementById(`tetris2`).classList.add("notThePlayer");
    }
  }
});

let btn = document.querySelector(".over");
btn.addEventListener("click", () => {
  socket.emit("gameOver", {
    idroom: document.getElementById("gameCodeDisplay").innerText,
    nbRed,
    nbYellow,
    nbBlue,
    nbGreen,
  });
  if (Math.max(nbRed, nbYellow, nbBlue, nbGreen) === nbRed) {
    alert(
      ` ${document.querySelector(".ScorePlayer1").textContent} is the winner `
    );
    window.location.reload();
  } else if (Math.max(nbRed, nbYellow, nbBlue, nbGreen) === nbYellow) {
    alert(
      ` ${document.querySelector(".ScorePlayer4").textContent} is the winner `
    );
    window.location.reload();
  } else if (Math.max(nbRed, nbYellow, nbBlue, nbGreen) === nbBlue) {
    alert(
      ` ${document.querySelector(".ScorePlayer3").textContent} is the winner `
    );
    window.location.reload();
  } else if (Math.max(nbRed, nbYellow, nbBlue, nbGreen) === nbGreen) {
    alert(
      ` ${document.querySelector(".ScorePlayer2").textContent} is the winner `
    );
    window.location.reload();
  }
});
socket.on("gameOverToClient", (data) => {
  if (
    Math.max(data.nbRed, data.nbYellow, data.nbBlue, data.nbGreen) ===
    data.nbRed
  ) {
    alert(
      ` ${document.querySelector(".ScorePlayer1").textContent} is the winner `
    );
    window.location.reload();
  } else if (
    Math.max(data.nbRed, data.nbYellow, data.nbBlue, data.nbGreen) ===
    data.nbYellow
  ) {
    alert(
      ` ${document.querySelector(".ScorePlayer4").textContent} is the winner `
    );
    window.location.reload();
  } else if (
    Math.max(data.nbRed, data.nbYellow, data.nbBlue, data.nbGreen) ===
    data.nbBlue
  ) {
    alert(
      ` ${document.querySelector(".ScorePlayer3").textContent} is the winner `
    );
    window.location.reload();
  } else if (
    Math.max(data.nbRed, data.nbYellow, data.nbBlue, data.nbGreen) ===
    data.nbGreen
  ) {
    alert(
      ` ${document.querySelector(".ScorePlayer2").textContent} is the winner `
    );
    window.location.reload();
  }
});

document.addEventListener("mouseover", drawHover);
document.addEventListener("mouseout", undrawOut);
document.addEventListener("click", checkAndDraw);

theShapChoseen(".oneshap0");
theShapChoseen(".oneshap1");
theShapChoseen(".oneshap2");
theShapChoseen(".oneshap3");

intialize();

// shapes in board
function intialize() {
  for (let r = 1; r <= 20; r++) {
    for (let c = 1; c <= 20; c++) {
      let tile = document.createElement("span");
      tile.id = r.toString() + "-" + c.toString();
      tile.classList.add("tile");
      // tile.innerText = tile.id;
      document.getElementById("board").appendChild(tile);
    }
  }
}

let RedFirstMove = document.getElementById(`1-1`);
RedFirstMove.classList.add("red");

let blueFirstMove = document.getElementById(`1-20`);
blueFirstMove.classList.add("blue");

let greenFirstMove = document.getElementById(`20-1`);
greenFirstMove.classList.add("green");

let yellowFirstMove = document.getElementById(`20-20`);
yellowFirstMove.classList.add("yellow");

//btn start game
let StartGame1 = function (e) {
  if (e.target.classList.contains("btnG")) {
    document.querySelector(".ScorePlayer1").innerText =
      document.getElementById("NamePlayer1").value;
    document.querySelector(".ScorePlayer2").innerText =
      document.getElementById("NamePlayer2").value;
    document.querySelector(".ScorePlayer3").innerText =
      document.getElementById("NamePlayer3").value;
    document.querySelector(".ScorePlayer4").innerText =
      document.getElementById("NamePlayer4").value;

    document.getElementById("playerOne").innerText =
      document.getElementById("NamePlayer1").value;
    document.getElementById("playerTwo").innerText =
      document.getElementById("NamePlayer2").value;
    document.getElementById("playerThree").innerText =
      document.getElementById("NamePlayer3").value;
    document.getElementById("playerFour").innerText =
      document.getElementById("NamePlayer4").value;

    if (
      document.getElementById("NamePlayer1").value !== "" &&
      document.getElementById("NamePlayer2").value !== "" &&
      document.getElementById("NamePlayer3").value !== "" &&
      document.getElementById("NamePlayer4").value !== ""
    ) {
      body = document.getElementsByTagName(`body`);
      bodyG = document.getElementById(`bodyG`);
      body[0].classList.remove("startG");
      body[0].classList.remove("ball");
      bodyG.classList.remove("bodyG");
      e.target.classList.add("displayBtnG");
      let ball = document.querySelectorAll(".ball");
      ball.forEach((b) => {
        b.remove();
      });
      boxNames1 = document.getElementsByClassName(`boxNames1`);
      boxNames1[0].classList.add("displayBoxNames1");
      boxNames1 = document.getElementsByClassName(`boxNames`);
      boxNames1[0].classList.add("displayBoxNames");
      rules = document.getElementsByClassName(`rules`);
      rules[0].classList.add("displayRules");
      document.getElementById("h4").classList.add("not-allowed");
    } else {
      alert("put your name");
    }
  }
};

document.addEventListener("click", StartGame1);

let newGameBtn = document.getElementById("newGameButton");
let joinGameBtn = document.getElementById("joinGameButton");
let gameCodeInput = document.getElementById("gameCodeInput");

function newGame() {
  socket.emit("newGame");

  let body = document.getElementsByTagName(`body`);
  let bodyG = document.getElementById(`bodyG`);
  body[0].classList.remove("startG");
  body[0].classList.remove("ball");
  bodyG.classList.remove("bodyG");
  // e.target.classList.add("displayBtnG");
  let ball = document.querySelectorAll(".ball");
  ball.forEach((b) => {
    b.remove();
  });
  let boxNames = document.getElementsByClassName(`boxNames`);
  boxNames[0].classList.add("displayBoxNames");
  let boxNames1 = document.getElementsByClassName(`boxNames1`);
  boxNames1[0].classList.add("displayBoxNames1");
  let rules = document.getElementsByClassName(`rules`);
  rules[0].classList.add("displayRules");
}

function joinGame() {
  let code = gameCodeInput.value;
  socket.emit("joinGame", code);
  let body = document.getElementsByTagName(`body`);
  let bodyG = document.getElementById(`bodyG`);
  body[0].classList.remove("startG");
  body[0].classList.remove("ball");
  bodyG.classList.remove("bodyG");
  // e.target.classList.add("displayBtnG");
  let ball = document.querySelectorAll(".ball");
  ball.forEach((b) => {
    b.remove();
  });
  let boxNames = document.getElementsByClassName(`boxNames`);
  boxNames[0].classList.add("displayBoxNames");
  let boxNames1 = document.getElementsByClassName(`boxNames1`);
  boxNames1[0].classList.add("displayBoxNames1");
  let rules = document.getElementsByClassName(`rules`);
  rules[0].classList.add("displayRules");
}

newGameBtn.addEventListener("click", newGame);
joinGameBtn.addEventListener("click", joinGame);

socket.on("id", (data) => {
  document.getElementById(`gameCodeDisplay`).innerText = data;
});

socket.on("roomName", (data) => {
  document.getElementById(`gameCodeDisplay`).innerText = data;
});
socket.on("maxplayer", (data) => {
  window.location.reload();
  alert(data);
});

// Some random colors
let colors = ["#3CC157", "#2AA7FF", "#1B1B1B", "#FCBC0F", "#F85F36"];

let numBalls = 50;
let balls = [];

for (let i = 0; i < numBalls; i++) {
  let ball = document.createElement("div");
  ball.classList.add("ball");
  ball.style.background = colors[Math.floor(Math.random() * colors.length)];
  ball.style.left = `${Math.floor(Math.random() * 100)}vw`;
  ball.style.top = `${Math.floor(Math.random() * 100)}vh`;
  ball.style.transform = `scale(${Math.random()})`;
  ball.style.width = `${Math.random()}em`;
  ball.style.height = ball.style.width;

  balls.push(ball);
  document.body.append(ball);
}
// Keyframes
balls.forEach((el, i, ra) => {
  let to = {
    x: Math.random() * (i % 2 === 0 ? -11 : 11),
    y: Math.random() * 12,
  };

  let anim = el.animate(
    [
      { transform: "translate(0, 0)" },
      { transform: `translate(${to.x}rem, ${to.y}rem)` },
    ],
    {
      duration: (Math.random() + 1) * 2000, // random duration
      direction: "alternate",
      fill: "both",
      iterations: Infinity,
      easing: "ease-in-out",
    }
  );
});

//give all player own color
let arr = [];
socket.on("createrColor", (data) => {
  if (socket.id === data) {
    document.getElementById("playerOne").style.color = "red";
    document.getElementById("tetris1").classList.add("none");
    document.getElementById("tetris2").classList.add("none");
    document.getElementById("tetris3").classList.add("none");
    alert("your color red ");
  }
});
socket.on("joinerColor", (data) => {
  if (data.nbClient[0] === 1) {
    alert("your color green");
    document.getElementById("playerTwo").style.color = "green";
    document.getElementById("tetris0").classList.add("none");
    document.getElementById("tetris2").classList.add("none");
    document.getElementById("tetris3").classList.add("none");
  }
  if (data.nbClient[0] === 2) {
    alert("your color blue ");
    document.getElementById("playerThree").style.color = "blue";
    document.getElementById("tetris1").classList.add("none");
    document.getElementById("tetris0").classList.add("none");
    document.getElementById("tetris3").classList.add("none");
  }
  if (data.nbClient[0] === 3) {
    alert("your color yellow ");
    document.getElementById("playerFour").style.color = "yellow";
    document.getElementById("tetris1").classList.add("none");
    document.getElementById("tetris2").classList.add("none");
    document.getElementById("tetris0").classList.add("none");
  }
});
